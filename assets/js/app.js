(function() {
    var app = angular.module('gallery', [
        'ngRoute',
        'galleryPhotos'
    ]);
    app.config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/partials/index.html'
            })
            .when('/detail/:photoId', {
                templateUrl: '/partials/detail.html'
            });
    }]);
})();
