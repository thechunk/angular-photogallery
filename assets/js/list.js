(function() {
    var app = angular.module('galleryPhotos', []);

    app.directive('photosUploadButton', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function() {
                    scope.fileUpload();
                });

                scope.$watch('fileUploading', function(value) {
                    if (value) {
                        element.html('Uploading...');
                    } else {
                        element.html('Upload');
                    }
                });
            }
        };
    });

    app.directive('photosUpload', function() {
        return {
            restrict: 'E',
            templateUrl: '/partials/photos-upload.html',
            controller: ['$http', '$scope', '$log', function($http, $scope, $log) {
                $scope.fileSelected = false;
                $scope.fileUploading = false;
                $scope.fileChanged = function(obj) {
                    if (obj.files.length > 0) {
                        $scope.fileSelected = true;
                        $scope.file = obj.files[0];
                    }
                };
                $scope.fileUpload = function() {
                    if (angular.isUndefined($scope.file)) {
                        alert('Please select a file');
                    }
                    var formData = new FormData();
                    formData.append('file', $scope.file);
                    $scope.fileUploading = true;
                    $http
                        .put('/photo/upload', formData, {
                            transformRequest: angular.identity,
                            headers: {
                                'Content-Type': undefined
                            }
                        })
                        .success(function(data) {
                            $scope.fileUploading = false;
                            $scope.$emit('listChanged', {
                                data: data.data
                            });
                            $log.info('uploaded');
                        })
                        .error(function(data) {
                            $scope.fileUploading = false;
                            $log.error('failed');
                        });

                    $log.info('upload ' + $scope.file.name);
                };
            }],
            controllerAs: 'uploadCtrl'
        };
    });

    app.directive('photosList', function() {
        return {
            restrict: 'E',
            templateUrl: '/partials/photos-list.html',
            controller: ['$http', '$scope', function($http, $scope) {
                var listCtrl = this;
                listCtrl.images = [];
                $http
                    .get('/photos/list')
                    .success(function(data) {
                        listCtrl.images = data.data;
                    })
                    .error(function(data) {
                    });

                $scope.$on('listChanged', function(event, args) {
                    listCtrl.images = args.data;
                });
            }],
            controllerAs: 'listCtrl'
        };
    });

    app.directive('photoDetail', function() {
        return {
            restrict: 'E',
            templateUrl: '/partials/photo-detail.html',
            controller: ['$http', '$routeParams', function($http, $routeParams) {
                var detailCtrl = this;
                detailCtrl.dimensions = {};
                detailCtrl.photoPath = '';
                detailCtrl.photoId = $routeParams.photoId;
                $http
                    .get('/photos/list/' + detailCtrl.photoId + '/details')
                    .success(function(data) {
                        detailCtrl.dimensions = data.data.dimensions;
                        detailCtrl.photoPath = data.data.fullPath;
                    })
                    .error(function(data) {
                    });
            }],
            controllerAs: 'detailCtrl'
        };
    });
})();
