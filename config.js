var config = {
    paths: {
        assets: '/assets',
        upload: '/img/upload'
    }
};

module.exports = config;
