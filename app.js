var crypto = require('crypto');
var fs = require('fs');
var express = require('express');
var multer = require('multer');
var sizeOf = require('image-size');

var config = require('./config');

var app = express();
var router = express.Router();

// config
app.use(multer({
    dest: __dirname + config.paths.assets + config.paths.upload,
    onFileUploadStart: function(file, data) {
        return file.mimetype.substring(0, 5) === 'image';
    },
    rename: function(fieldname, filename) {
        var buf = crypto.randomBytes(24);
        return buf.toString('hex');
    }
}));

// routing
router.get('/photos/list', function(req, res) {
    var files = fs.readdirSync(__dirname + config.paths.assets + config.paths.upload);
    res.send({
        data: files.map(function(file) {
            if (file.substring(0, 1) === '.')
                return false;
            return {
                fileName: file,
                fullPath: config.paths.upload + '/' + file
            };
        })
    });
});
router.get('/photos/list/:id/details', function(req, res) {
    var file = fs.readFileSync(__dirname + config.paths.assets + config.paths.upload + '/' + req.params.id);
    var dimensions = sizeOf(file);

    res.send({
        data: {
            fullPath: config.paths.upload + '/' + req.params.id,
            dimensions: dimensions
        }
    });
});
router.put('/photo/upload', function(req, res) {
    var files = fs.readdirSync(__dirname + config.paths.assets + config.paths.upload);

    res.send({
        data: files.map(function(file) {
            if (file.substring(0, 1) === '.')
                return false;
            return {
                fileName: file,
                fullPath: config.paths.upload + '/' + file
            };
        })
    });
});

app.use(router);
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/assets'));

var server = app.listen(3000, function() {
    var host = server.address().address,
        port = server.address().port;
    console.log('%s:%s', host, port);
});
